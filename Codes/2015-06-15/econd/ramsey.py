#!/usr/bin/env python3
# -*- coding:utf-8 -*-

"""Simulation of a Ramsey model"""

import matplotlib.pyplot as plt
import numpy as np

from econd.system import InvertibleSystem, DifferentiableSystem, LinearSystem, \
                    linearize, Simulation, find_initial_near_eqm, quiver_plot

class Ramsey(InvertibleSystem, DifferentiableSystem):
    """One-sector Ramsey model"""

    def __init__(self, A, α, ρ):
        super().__init__(dim=2)
        self.A, self.α, self.ρ = A, α, ρ

    def f(self, x):
        """Production function"""
        return self.A * x ** self.α

    def U(self, x):
        """Utility from consumption"""
        return np.log(x)

    def u(self, x, y):
        """Reduced form utility"""
        return self.U(self.f(x) - y)

    def is_feasible(self, x, y):
        """Checks feasibility"""
        return x >= 0 and y >= 0 and self.f(x) >= y

    def steady_state(self):
        """Steady state for the model"""
        A, α, ρ = self.A, self.α, self.ρ
        koo = (ρ * α * A) ** (1 / (1 - α))
        return np.array([koo, koo])

    def forward(self, x):
        """Forward evolution"""
        A, α, ρ = self.A, self.α, self.ρ
        return np.array([
            x[1],
            ((1 + ρ * α) * A * x[1] ** α -
             ρ * α * (A ** 2) * (x[1] ** (α - 1)) * (x[0] ** α))
        ])

    def reverse(self, x):
        """Backward evolution"""
        A, α, ρ = self.A, self.α, self.ρ
        return np.array([
            ((((1 + ρ * α) * A * x[0] ** α - x[1]) /
              (ρ * α * A ** 2 * x[0] ** (α - 1))) ** (1 / α)),
            x[0]
        ])

    def jacobian(self, x=None):
        """Returns jacobian matrix"""
        A, α, ρ = self.A, self.α, self.ρ

        def _jacobian(x):
            """Jacobian as a function of state"""
            return  np.array([
                [0, 1],
                [-ρ * ((α * A) ** 2) * ((x[0] * x[1]) ** (α - 1)),
                 (α * (1 + ρ * α) * A * x[0] ** (α - 1) -
                  ρ * α * (α-1) * (A ** 2) * (x[0]**α) * (x[1] ** (α-2)))]
            ])

        if x is None:
            return _jacobian(self.steady_state())
        return _jacobian(x)


if __name__ == "__main__":

    ramsey = Ramsey(A=1.2, α=0.4, ρ=0.98)
    linearized = linearize(ramsey, ramsey.steady_state())

    sim_ramsey = Simulation(ramsey, domain=lambda x: ramsey.is_feasible(*x))
    sim_linear = Simulation(linearized)

    fig = plt.figure()
    ax = fig.add_subplot(111)

    kmax = 1.8 * ramsey.steady_state()[0]
    k = np.linspace(0.0, kmax, 200)
    ax.plot(k, ramsey.f(k), color='black')
    ax.fill_between(k, ramsey.f(k), 0.0, color='black', alpha=0.1)
    ax.plot(k, k, color='black', linestyle='dashed')

    init_vals = find_initial_near_eqm(linearized, eps=1e-6)
    for lambda_, inits in init_vals:
        if abs(lambda_) < 1:
            path = list(sim_linear.reset(inits, 15, inverse=True))
            path.reverse()
        else:
            path = list(sim_linear.reset(inits, 20, inverse=False))
        quiver_plot(ax, path, width=0.004, color='black')

    for lambda_, inits in init_vals:
        if abs(lambda_) < 1:
            path = list(sim_ramsey.reset(inits, 16, inverse=True))
            path.reverse()
        else:
            path = list(sim_ramsey.reset(inits, 16, inverse=False))
        quiver_plot(ax, path, width=0.004, color='red')

    ax.set_xlim([0, kmax])
    ax.set_ylim([0, ramsey.f(k[-1])])
    plt.show()

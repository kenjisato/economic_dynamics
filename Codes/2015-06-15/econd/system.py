"""Classes for Systems and Simulation"""

import abc
import numpy as np
import scipy.linalg as la
import itertools

class System(abc.ABC):
    """System base class"""
    def __init__(self, dim):
        self.dim = dim

    @abc.abstractmethod
    def forward(self, x):
        """Map from x_t to x_{t+1}"""


class InvertibleSystem(System, abc.ABC):
    """Invertible sytem base class"""
    @abc.abstractmethod
    def reverse(self, x):
        """Map from x_{t+1} to x_t"""


class DifferentiableSystem(System, abc.ABC):
    """Differentiable system base class"""

    @abc.abstractmethod
    def jacobian(self, x):
        """Jacobian matrix for the system"""


class LinearSystem(InvertibleSystem):
    """Linear/Affine system"""

    def __init__(self, A, origin=None):
        self.A = A
        super().__init__(A.shape[0])
        self.origin = np.zeros(self.dim) if origin is None else np.asarray(origin)

    def forward(self, x):
        return np.dot(self.A, np.asarray(x) - self.origin) + self.origin

    def reverse(self, x):
        return la.solve(self.A, np.asarray(x) - self.origin) + self.origin

    def eig(self):
        """Eigen values and eigen vectors"""
        return la.eig(self.A)


def linearize(diffsys, ss):
    """Linearize differentiable system around a steady state"""
    if not np.allclose(diffsys.forward(ss), ss):
        raise ValueError("Not a steady state")
    return LinearSystem(diffsys.jacobian(ss), ss)


class Simulation:
    """Simulation of a dynamical system"""

    def __init__(self, system, x0=None, duration=np.Inf, inverse=False, domain=None):
        self.system = system
        self.x0 = x0
        self.duration = duration
        self.inverse = inverse
        if domain is not None:
            self.domain = domain
        else:
            self.domain = lambda x: True

    def __iter__(self):
        x = self.x0[:]
        tick = self.system.forward if not self.inverse else self.system.reverse
        t = itertools.count()
        while next(t) < self.duration and self.domain(x):
            yield x
            x = tick(x)
        del t

    def reset(self, x0=None, duration=None, inverse=None):
        """Reset simulation parameters"""

        if x0 is not None:
            self.x0 = x0[:]
        if duration is not None:
            self.duration = duration
        if inverse is not None:
            self.inverse = inverse
        return self

    def __repr__(self):
        text = "Simulation({},\n"
        text += "           x0={},\n"
        text += "           duration={})"
        return text.format(
            str(self.system),
            str(self.x0),
            str(self.duration)
        )


def find_initial_near_eqm(linsys, eps=1e-5):
    init_vals = []
    signs = [-1, 1]
    lambdas, V = linsys.eig()
    for i, lambda_ in enumerate(lambdas):
        for sign in signs:
            init_vals.append((lambda_, linsys.origin + sign * eps * V[:, i]))
    return init_vals

def quiver_plot(ax, path, **kwargs):
    options = {
        'scale_units': 'xy',
        'angles': 'xy',
        'scale': 1,
        'width': 0.003,
        'color': 'black'
    }
    options.update(kwargs)
    path = np.asarray(path)
    return ax.quiver(path[:-1, 0], path[:-1, 1],
                     path[1:, 0]-path[:-1, 0], path[1:, 1]-path[:-1, 1], **options)


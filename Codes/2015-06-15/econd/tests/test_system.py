import unittest
import numpy as np
import system

class TestLinearSystem(unittest.TestCase):
    def setUp(self):
        self.A = np.array([[2.0, 0.0],
                           [0.0, 1.0]])
        self.x = np.array([0.5, 1.0])
        self.linsys = system.LinearSystem(self.A)

    def test_forward(self):
        self.assertTrue(np.allclose(self.linsys.forward(self.x), np.ones(2)))

    def test_reverse(self):
        self.assertTrue(np.allclose(self.linsys.reverse(self.x), np.array([0.25, 1.0])))

    def test_eig(self):
        lambdas, V = self.linsys.eig()
        for i, lambda_ in enumerate(lambdas):
            self.assertTrue(np.allclose(np.dot(self.A, V[:, i]), lambda_ * V[:, i]))


if __name__ == "__main__":
    unittest.main()


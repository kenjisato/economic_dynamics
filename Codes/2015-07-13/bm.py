import numpy as np
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt


class BoldrinMontrucchio:

    def __init__(self):
        self.ρ = 0.01072

    def u(self, x, y):
        return (-0.0857 * y**4 + 0.1715 * y**3 - 0.3285 * y**2 -
                1.61 * y + 4 * x * y * (1 - x) - 24 * x**2 +
                150 * x)

    def is_feasible(self, x, y):
        return True


class PLApprox:
    def __init__(self, a, b, N, upsample=10):
        self.a, self.b, self.N = a, b, N
        self.grids = np.linspace(a, b, upsample*N)
        self.centers = np.linspace(a, b, N)

    def proj(self, f):
        return np.array([f(c) for c in self.centers])

    def inj(self, a):
        return interp1d(self.centers, a)


def value_of_policy(f, model, apx, T):
    fc = apx.inj(f)
    X = apx.centers
    Y = fc(X)
    vf = np.zeros_like(X)
    for i in range(T):
        vf += model.ρ**i * model.u(X, Y)
        X, Y = Y, fc(Y)
    return vf


def greedy(vf, model, apx):
    vc = apx.inj(vf)
    Y = apx.grids
    fhat = np.empty_like(vf)
    for i, x in enumerate(apx.centers):
        X = np.ones_like(Y) * x
        with np.errstate(invalid='ignore'):
            maximand = np.where(model.is_feasible(X, Y),
                                model.u(X, Y) + model.ρ*vc(Y),
                                -np.inf)
        fhat[i] = Y[np.argmax(maximand)]
    return fhat


if __name__ == '__main__':
    model = BoldrinMontrucchio()
    apx = PLApprox(a=0.0, b=1.0, N=500, upsample=100)
    df = apx.proj(lambda x: x)

    fhat = df
    for _ in range(10):
        vf = value_of_policy(fhat, model, apx, T=100)
        fhat = greedy(vf, model, apx)

    for _ in range(3):
        vf = value_of_policy(fhat, model, apx, T=100)
        fhat = greedy(vf, model, apx)

    fig = plt.figure()
    ax = fig.add_subplot(111, aspect='equal')
    plt.plot(apx.centers, fhat)
    plt.savefig('bm.pdf', bbox_inches='tight')

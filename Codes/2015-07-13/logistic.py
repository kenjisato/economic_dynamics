from sympy import Lambda, sqrt, expand, pprint, simplify
from sympy.abc import x, y, a, r, L

gamma, sigma, mu, N = 4, 8, 1, 1

rho_star = gamma**2 + mu*sigma - gamma*sqrt(gamma**2 + 2*mu*sigma)
rho_star /= 2 * (mu**2) * (sigma**2)
rho = rho_star.round(5)

alpha = (1 - (-2 * gamma**2 +
         2 * gamma * sqrt(gamma**2 + 2*mu*sigma)) /
         (4 * mu * sigma)).round(10)
beta = alpha / rho_star.round(10)
L = beta - mu * sigma
a = 150

# Lemma 2
assert L >= gamma ** 2 / (1 - alpha) + mu * sigma

# Lemma 3
assert beta >= L + mu * sigma

# Eq. 6
assert alpha - rho * beta > 0

# Theorem 3 (Inequality below Eq. 9)
assert a > L * N + gamma * mu + N / rho

f = Lambda(x, 4 * x * (1 - x))
w = Lambda((x, y), - y**2 / 2 + y * f(x) - L * x**2 / 2)
w_ = Lambda((x, y), w(x, y) + a * x)

u = Lambda((x, y), w(x, y) - r * w(y, f(y)))
u_ = simplify(expand(Lambda((x, y), w_(x, y) - r * w_(y, f(y)))))

print('u_ is \n{}'.format(u_.subs(r, rho)))
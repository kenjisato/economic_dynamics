from os.path import join
from itertools import islice

import numpy as np
from scipy.optimize import fsolve
import matplotlib.pyplot as plt


class LogisticMap:
    """Logistic map on [0, 1]."""
    def __init__(self, a):
        self.a = a

    def __call__(self, x):
        return self.a * x * (1.0 - x)


class SkewedTentMap:
    """A two-piece piecewise linear map on [0, 1].
    First slope is a > 0, the second slope is b < -1.
    """
    def __init__(self, a, b):
        if not a > 0:
            raise ValueError("a must satisfy a > 0")
        if not b < -1:
            raise ValueError("b must satisfy b < -1")

        self.a, self.b = a, b
        self.peak = 1.0 + 1.0 / b
        self.intercept = 1.0 - self.peak * a

        self.fixed_point = b / (b - 1.0)

    def __call__(self, x):
        return np.where(x < self.peak,
                        self.a * x + self.intercept,
                        self.b * (x - 1.0))


def iterate(f, n):
    """n-th iterate of f"""
    if n == 1:
        return f
    else:
        return lambda x: f(iterate(f, n - 1)(x))

def trajectory(f, x0, t):
    x, cnt = x0, 0
    while cnt < t:
        yield x
        x = f(x)
        cnt += 1


class Plot:
    """A class for easy plotting"""

    def __init__(self, fname=None,
                 aspect='equal', save_option={},
                 x=(0.0, 1.0, 1000)):

        self.x = np.linspace(*x)
        self.f = None
        self.fname = fname
        self.aspect = aspect

        if len(save_option) == 0:
            self.save_option = {
                'bbox_inches': 'tight'
            }
        else:
            self.save_option = save_option

    def __enter__(self):

        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111, aspect=self.aspect)
        return self

    def __exit__(self, exc_type, exc_value, traceback):

        if self.fname is None:
            plt.show()
        else:
            plt.savefig(self.fname, **self.save_option)
            plt.close()

    def draw(self, **kwargs):
        return self.draw_func(self.f, **kwargs)

    def draw_func(self, f, **kwargs):
        x, ax = self.x, self.ax
        return ax.plot(x, f(x), **kwargs)

    def set_func(self, f):
        self.f = f

    def draw_45(self, **kwargs):
        return self.ax.plot(self.x, self.x, **kwargs)

    def draw_3period(self, x0=0.2, **kwargs):
        f, ax = self.f, self.ax

        f3 = iterate(f, 3)

        p0 = fsolve(lambda x: f3(x) - x, x0)
        p1 = f(p0)
        p2 = f(p1)
        assert not np.allclose(p0, p1)
        assert not np.allclose(p1, p2)
        assert np.allclose(p0, f(p2))

        # TODO: VERIFY
        ax.axvline(p0, p0, p1, **kwargs)
        ax.axhline(p1, p0, p1, **kwargs)
        ax.axvline(p1, p1, p2, **kwargs)
        ax.axhline(p2, p1, p2, **kwargs)
        ax.axvline(p2, p0, p2, **kwargs)
        ax.axhline(p0, p0, p2, **kwargs)

        ax.plot(p0, p0, 'ko')
        ax.plot(p1, p1, 'ko')
        ax.plot(p2, p2, 'ko')

    def draw_iterate(self, n, **kwargs):
        f, x, ax = self.f, self.x, self.ax
        return ax.plot(x,
                       iterate(f, n)(x), **kwargs)


    def draw_trajectory(self, x0, t, **kwargs):
        f, ax = self.f, self.ax
        trajectory = [x0]
        for _ in range(t-1):
            trajectory.append(x0)
            x0 = f(x0)
        return ax.plot(trajectory, **kwargs)

    def logistic_3period(self, a=4.0,
                         fname=None, save_option={}):
        self.f = LogisticMap(a)
        ax = self.ax


        l1, = self.draw(ax, color='red', lw=1.3)
        l3, = self.draw_iterate(3, ax, color='blue', ls='--')
        self.draw_45(ax, color='black', linestyle='dotted')

        ax.legend((l1, l3),
                  ('logistic', '3rd iterate'),
                  loc='lower right')

    def __getattr__(self, name):
        return getattr(self.ax, name)

if __name__ == "__main__":

    root = '../../Handouts/fig'

    logistic = LogisticMap(4.0)
    tent = SkewedTentMap(0.7, -3.0)

    with Plot(join(root, 'logistic.pdf')) as p:
        p.draw_45(color='black', linestyle='--')
        for a in np.linspace(2.8, 4.0, 10):
            p.set_func(LogisticMap(a))
            p.draw()

    with Plot(join(root, 'bifurcation.png'), aspect='auto',
              save_option={'bbox_inches': 'tight', 'dpi': 196}) as p:
        x0 = 0.2
        for a in np.linspace(2.7, 4.0, 400):
            logis = LogisticMap(a)
            it = trajectory(logis, 0.2, 450)
            limit = list(islice(it, 300, None))
            param = [a for _ in limit]

            p.set_xlabel('$a$', fontsize=20)
            p.set_ylabel('$x$', fontsize=20)
            p.plot(param, limit, color='black', marker='.',
                   markersize=2, linestyle='')

    with Plot(join(root, 'three_period.pdf')) as p:
        p.draw_45(color='black', linestyle='--')

        p.set_func(logistic)
        p.draw()
        p.draw_3period(x0=0.2, color='orange')
        p.draw_3period(x0=0.1, color='green')

    with Plot(join(root, 'trajectory.pdf'), aspect='auto') as p:
        p.set_func(logistic)
        p.set_ylabel('$x$', fontsize=20)
        p.set_xlabel('$t$', fontsize=20)
        p.draw_trajectory(x0=0.1, t=80,
                          marker='o', linestyle=':')

    with Plot(join(root, 'sensitivity.pdf'), aspect='auto') as p:
        p.set_func(logistic)
        p.set_ylabel('$x$', fontsize=20)
        p.set_xlabel('$t$', fontsize=20)
        p.draw_trajectory(x0=0.1, t=80,
                          marker='o', alpha=0.5)
        p.draw_trajectory(x0=0.1000001, t=80,
                          marker='o', alpha=0.5)
        p.draw_trajectory(x0=0.1000002, t=80,
                          marker='o', alpha=0.5)

    with Plot(join(root, 'third_iterate.pdf')) as p:
        p.draw_45(color='black', linestyle='--')
        p.set_func(logistic)
        p.draw_iterate(3)

    with Plot(join(root, 'mitra.pdf')) as p:
        p.draw_45(color='black', linestyle='--')
        p.set_func(tent)

        p.axvline(tent.peak, 0.0, 1.0, linestyle=':', color='red')
        p.axhline(tent.intercept, 0.0, 1.0, linestyle=':', color='green')
        p.axhline(tent.fixed_point, 0.0, 1.0, linestyle=':', color='green')

        p.text(tent.peak+0.01, 0.01, '$m$', fontsize=20)
        p.text(0.01, tent.intercept-0.07, '$f^3(m)$', fontsize=20)
        p.text(0.01, tent.fixed_point+0.01, '$z$', fontsize=20)

        p.draw()

    with Plot(join(root, 'a-concave.pdf'), aspect='auto') as p:
        def cf(x): return -(x-0.8)**2 + 0.8**2

        p.set_func(cf)
        p.draw(linewidth=2, color='black')

        for a in np.linspace(3.0, 0.1, 5):
            p.set_func(lambda x: cf(x) + 0.5 * a * x**2)
            line, = p.draw()
            line.set_label("$\\alpha={}$".format(a))
        p.legend(loc='upper left')


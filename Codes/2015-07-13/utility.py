import numpy as np
import matplotlib.pyplot as plt

class W:
    def __init__(self, theta, L, kbar, a):
        self.theta = theta
        self.L = L
        self.kbar = kbar
        self.a = a

    def __call__(self, x, y):
        return (-0.5 * y**2 + (y - self.kbar) * self.theta(x) -
                0.5 * self.L * x**2 + self.a*x)


class U:
    def __init__(self, w, rho):
        self.w = w
        self.theta = w.theta
        self.rho = rho

    def __call__(self, x, y):
        return self.w(x, y) - rho * self.w(y, self.theta(y))


def logistic(x):
    return 4.0 * x * (1.0 - x)


if __name__ == "__main__":

    gamma = 4.0
    sigma = 8.0
    mu = 1.0

    rho_star = gamma**2 + mu*sigma - gamma*np.sqrt(gamma**2 + 2*mu*sigma)
    rho_star /= 2 * (mu**2) * (sigma**2)

    rho = round(rho_star, 5)


    alpha = 0.9
    L = (gamma * 2) / (1.0 - alpha) + mu * sigma
    beta = L + mu * sigma

    w = W(logistic, L=48, kbar=0.0, a=150)
    u = U(w, rho)

    x = np.linspace(0.0, 1.0, 100)
    y = np.linspace(0.0, 1.0, 100)
    X, Y = np.meshgrid(x, y)

    fig = plt.figure()
    ax = fig.add_subplot(111, aspect='equal')
    c = ax.contour(X, Y, u(X, Y),
                   levels=np.linspace(0, u(1.0,0), 15),
                   colors='blue')
    plt.clabel(c, inline=1, fontsize=10)

    plt.show()


import numpy as np
import matplotlib.pyplot as plt
from collections import namedtuple
from listing05 import Ramsey


class Simulation:
    """Simulation of a dynamical system"""

    def __init__(self, sys, x0=None, duration=None):
        self.sys = sys
        self.x0 = x0
        self.duration = duration

    def __iter__(self):
        x = self.x0[:]
        for _ in range(self.duration):
            yield x
            x = self.sys.forward(x)

    def reset(self, *, x0=None, duration=None):
        if x0 is not None:
            self.x0 = x0[:]
        if duration is not None:
            self.duration = duration

    def plot(self, ax):
        for i, x in enumerate(self):
            if i == 0:
                ax.plot(x[0], x[1], marker='', linestyle='', color='black')
            else:
                dx = [x[0] - x0[0], x[1] - x0[1]]
                ax.plot(x[0], x[1], marker='', linestyle='', color='black')
                ax.arrow(x0[0], x0[1], dx[0], dx[1], length_includes_head=True)
            x0 = x[:]


if __name__ == "__main__":

    ramsey = Ramsey(A=1.1, α=0.4, ρ=0.9)
    sim = Simulation(ramsey)

    SimParam = namedtuple('SimParam', ('x0', 'duration'))
    params = [
        SimParam((1.2, 0.460), 10),
        SimParam((1.2, 0.430), 10),
        SimParam((1.2, 0.425), 6),
        SimParam((0.05, 0.15), 10)
    ]

    fig, ax = plt.subplots(subplot_kw={'aspect':'equal'})

    grids = np.linspace(0.0, 1.2, 120)
    ax.plot(grids, ramsey.f(grids))

    for param in params:
        sim.reset(x0=param.x0, duration=param.duration)
        sim.plot(ax)




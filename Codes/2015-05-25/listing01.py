#!/usr/bin/env python3

from math import log

## Unicode identifiers are only valid in Python3
## In Python2, use less readable notation such as
# alpha = 0.4
# rho = 0.9

A = 1.1
α = 0.4
ρ = 0.9


def f(k):
    """Production function"""
    return A * k ** α


def U(c):
    """Utility function"""
    return log(c)

def u(x, y):
    """Reduced form utility function"""
    return U(f(x) - y)


def F(x, y):
    """Solution of Euler equation"""
    return ((1 + ρ * α) * A * y ** α
            - ρ * α * (A ** 2) * (x ** α) * (y ** (α - 1)))


def G(x):
    """Dynamical system"""
    return [
            x[1],
            F(x[0], x[1])
        ]

if __name__ == "__main__":

    duration = 4
    x0 = [0.8, 0.43]

    x = x0[:]
    for t in range(duration):
        print(x)
        x = G(x)

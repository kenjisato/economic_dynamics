#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from listing01 import f, G

duration = 10
x0 = [0.8, 0.43]

fig = plt.figure()
ax = fig.add_subplot(111, aspect='equal')

grids = np.linspace(0.0, 1.2, 120)
ax.plot(grids, f(grids))


x = x0[:]
for t in range(duration):
    x1 = G(x)
    dx = [x1[0] - x[0], x1[1] - x[1]]
    ax.plot(x[0], x[1], marker='', linestyle=' ', color='black')
    ax.arrow(x[0], x[1], dx[0], dx[1], length_includes_head=True)
    x = x1
plt.show()

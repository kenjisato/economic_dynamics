#!/usr/bin/env python3

import matplotlib.pyplot as plt
from listing01 import f, G

duration = 10
x0 = [0.8, 0.43]

fig = plt.figure()
ax = fig.add_subplot(111)

grids = np.linspace(0.0, 1.2, 120)
ax.plot(grids, f(grids))

x = x0[:]
for t in range(duration):
    ax.plot(x[0], x[1], marker='o', linestyle=' ')
    x = G(x)

plt.show()

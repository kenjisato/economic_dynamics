#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

class Ramsey:
    """One-sector Ramsey model"""

    def __init__(self, A, α, ρ):
        self.A = A
        self.α = α
        self.ρ = ρ

    def f(self, x):
        return self.A * x ** self.α

    def U(self, x):
        return log(x)

    def u(self, x, y):
        return self.U(self.f(x) - y)

    def forward(self, x):
        """1ステップの時間発展"""
        A, α, ρ = self.A, self.α, self.ρ
        return [
            x[1],
            (1 + ρ * α) * A * x[1] ** α - ρ * α * (A ** 2) * (x[1] ** (α - 1)) * (x[0] ** α)
        ]


class Simulation:
    """Simulation of a dynamical system"""

    def __init__(self, sys, x0, duration):
        self.sys = sys
        self.x0 = x0
        self.duration = duration

    def __iter__(self):
        x = self.x0[:]
        for _ in range(self.duration):
            yield x
            x = self.sys.forward(x)


if __name__ == "__main__":

    ramsey = Ramsey(A=1.1, α=0.4, ρ=0.9)
    sim = Simulation(ramsey, x0=[0.8, 0.43], duration=10)

    fig, ax = plt.subplots(subplot_kw={'aspect':'equal'})

    grids = np.linspace(0.0, 1.2, 120)
    ax.plot(grids, ramsey.f(grids))

    for i, x in enumerate(sim):
        if i == 0:
            ax.plot(x[0], x[1], marker='', linestyle='', color='black')
        else:
            dx = [x[0] - x0[0], x[1] - x0[1]]
            ax.plot(x[0], x[1], marker='', linestyle='', color='black')
            ax.arrow(x0[0], x0[1], dx[0], dx[1], length_includes_head=True)
        x0 = x[:]


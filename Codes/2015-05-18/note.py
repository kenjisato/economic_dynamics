# Generators

def logistic(x):
    """Logistic map"""
    return 4 * x * (1 - x)
    
def logistic_path(x, length):
    y = [x]
    for _ in range(length - 1):
        y.append(logistic(y[-1]))
    return y

def logistic_gen(x, length):
    for _ in range(length):
        x1 = logistic(x)
        yield x
        x = x1
        

    
import numpy as np

class System:
    def __init__(self, dim):
        self.dim = dim
        
    def forward(self, x):
        pass
        
class Logistic(System):
    def __init__(self, a):
        self.a = a
        super().__init__(dim=1)
        # System.__init__(dim=1)
        
    def forward(self, x):
        return self.a * x * (1 - x)
    
class Tent(System):
    def __init__(self, a):
        self.a = a
        super().__init__(dim=1)
        
    def forward(self, x):
        return self.a * min(x, 1 - x)
    

class Linear(System):
    def __init__(self, A):
        self.A = A
        super().__init__(dim=A.shape[0])
    
    def forward(self, x):
        return np.dot(self.A, x)

class NoUse:
    def forward(self, x):
        return 2 * x     
        
def run(system, x, steps):
    for i in range(steps):
        x1 = system.forward(x)
        yield x
        x = x1
        
        
        
        
        
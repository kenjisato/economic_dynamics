import numpy as np

class System:
	def __init__(self, f, dim):
		self.f = f
		self.dim = dim

	def forward(self, x):
		return self.f(x)

def simulate(s, *, start, steps):

	x = np.array(start)
	count = 0

	while count < steps:
		yield x
		x = s.forward(x)
		count += 1


def shoot(s, *, goal, steps):

	for 


if __name__ == "__main__":

	def f(x):
		A = np.array([[1.0, -0.5], [0.0, 1.0]])
		return np.dot(A, x)

	s = System(f, (2, 2))

	path = simulate(s, start=[1., 1.], steps=10)



import numpy as np
import matplotlib.pyplot as plt

x = np.random.random(100)
noise = np.random.randn(100)
y = 2 * x + noise

plt.plot(x, y, marker="o", linestyle='')
plt.show()